﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClearCache1C
{
    public partial class FormResult : Form
    {
        public FormResult()
        {
            StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
        }

        private void FormResult_Load(object sender, EventArgs e)
        {
            MaximizeBox = false;
            MinimizeBox = false;
        }
        public void SetInfoResult(int successfully_deleted, int errors_on_delete, float total_size_deleted, 
            List<string> list_success, List<string> list_error, List<string> list_exceptions)
        {
            InfoResult.ResetText();
            InfoResult.AppendText("--- Итоги ---");
            InfoResult.AppendText("\r\nВыполнено успешно: " + successfully_deleted);
            InfoResult.AppendText("\r\nВыполнено с ошибками: " + errors_on_delete);
            InfoResult.AppendText("\r\nВсего удалено: " + total_size_deleted.ToString("0.00") + " Mb");
            InfoResult.AppendText("\r\n");
            InfoResult.AppendText("\r\n--- Список обработанных успешно ---");
            foreach (string s in list_success)
            {
                InfoResult.AppendText("\r\n > " + s);
            }
            InfoResult.AppendText("\r\n");
            InfoResult.AppendText("\r\n--- Список обработанных с ошибками ---");
            foreach (string s in list_error)
            {
                InfoResult.AppendText("\r\n > " + s);
            }
            InfoResult.AppendText("\r\n");
            InfoResult.AppendText("\r\n--- Лог ошибок ---");
            foreach (string s in list_exceptions)
            {
                InfoResult.AppendText("\r\n > " + s);
            }
        }
    }
}
