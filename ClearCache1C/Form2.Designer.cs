﻿namespace ClearCache1C
{
    partial class FormResult
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.InfoResult = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // InfoResult
            // 
            this.InfoResult.BackColor = System.Drawing.SystemColors.Info;
            this.InfoResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InfoResult.Location = new System.Drawing.Point(0, 0);
            this.InfoResult.Multiline = true;
            this.InfoResult.Name = "InfoResult";
            this.InfoResult.ReadOnly = true;
            this.InfoResult.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.InfoResult.Size = new System.Drawing.Size(464, 461);
            this.InfoResult.TabIndex = 0;
            // 
            // FormResult
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(464, 461);
            this.Controls.Add(this.InfoResult);
            this.Name = "FormResult";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Результат удаления";
            this.Load += new System.EventHandler(this.FormResult_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox InfoResult;
    }
}