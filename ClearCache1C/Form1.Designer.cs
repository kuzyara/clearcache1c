﻿namespace ClearCache1C
{
    partial class FormMain
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.ListBases = new System.Windows.Forms.CheckedListBox();
            this.btnClearCache = new System.Windows.Forms.Button();
            this.btnPurgeAll = new System.Windows.Forms.Button();
            this.InfoBox = new System.Windows.Forms.TextBox();
            this.btnExpand = new System.Windows.Forms.Button();
            this.btnSmartSelect = new System.Windows.Forms.Button();
            this.btnRemoveSelection = new System.Windows.Forms.Button();
            this.btnSelectAll = new System.Windows.Forms.Button();
            this.btnAbout = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ListBases
            // 
            this.ListBases.BackColor = System.Drawing.SystemColors.Window;
            this.ListBases.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.ListBases.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ListBases.FormattingEnabled = true;
            this.ListBases.Location = new System.Drawing.Point(12, 42);
            this.ListBases.Name = "ListBases";
            this.ListBases.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ListBases.Size = new System.Drawing.Size(400, 364);
            this.ListBases.TabIndex = 0;
            this.ListBases.SelectedIndexChanged += new System.EventHandler(this.ListBases_SelectedIndexChanged);
            // 
            // btnClearCache
            // 
            this.btnClearCache.Location = new System.Drawing.Point(12, 412);
            this.btnClearCache.Name = "btnClearCache";
            this.btnClearCache.Size = new System.Drawing.Size(198, 37);
            this.btnClearCache.TabIndex = 1;
            this.btnClearCache.Text = "Очистить кэш";
            this.btnClearCache.UseVisualStyleBackColor = true;
            this.btnClearCache.Click += new System.EventHandler(this.ButtonClearCache_Click);
            // 
            // btnPurgeAll
            // 
            this.btnPurgeAll.Location = new System.Drawing.Point(214, 412);
            this.btnPurgeAll.Name = "btnPurgeAll";
            this.btnPurgeAll.Size = new System.Drawing.Size(198, 37);
            this.btnPurgeAll.TabIndex = 4;
            this.btnPurgeAll.Text = "Удалить все";
            this.btnPurgeAll.UseVisualStyleBackColor = true;
            this.btnPurgeAll.Click += new System.EventHandler(this.ButtonPurgeAll_Click);
            // 
            // InfoBox
            // 
            this.InfoBox.BackColor = System.Drawing.SystemColors.Info;
            this.InfoBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.InfoBox.Location = new System.Drawing.Point(445, 12);
            this.InfoBox.Multiline = true;
            this.InfoBox.Name = "InfoBox";
            this.InfoBox.ReadOnly = true;
            this.InfoBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.InfoBox.Size = new System.Drawing.Size(400, 437);
            this.InfoBox.TabIndex = 6;
            this.InfoBox.WordWrap = false;
            // 
            // btnExpand
            // 
            this.btnExpand.FlatAppearance.BorderSize = 0;
            this.btnExpand.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExpand.Location = new System.Drawing.Point(418, 42);
            this.btnExpand.Name = "btnExpand";
            this.btnExpand.Size = new System.Drawing.Size(21, 364);
            this.btnExpand.TabIndex = 7;
            this.btnExpand.Text = ">\r\n>\r\n>";
            this.btnExpand.UseVisualStyleBackColor = true;
            this.btnExpand.Click += new System.EventHandler(this.ButtonExpand_Click);
            // 
            // btnSmartSelect
            // 
            this.btnSmartSelect.Image = global::ClearCache1C.Properties.Resources.icon_smart_select_16;
            this.btnSmartSelect.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSmartSelect.Location = new System.Drawing.Point(12, 12);
            this.btnSmartSelect.Name = "btnSmartSelect";
            this.btnSmartSelect.Size = new System.Drawing.Size(130, 27);
            this.btnSmartSelect.TabIndex = 8;
            this.btnSmartSelect.Text = "Выделить с кэшем";
            this.btnSmartSelect.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSmartSelect.UseVisualStyleBackColor = true;
            this.btnSmartSelect.Click += new System.EventHandler(this.ButtonSmartSelect_Click);
            // 
            // btnRemoveSelection
            // 
            this.btnRemoveSelection.Image = ((System.Drawing.Image)(resources.GetObject("btnRemoveSelection.Image")));
            this.btnRemoveSelection.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRemoveSelection.Location = new System.Drawing.Point(282, 12);
            this.btnRemoveSelection.Name = "btnRemoveSelection";
            this.btnRemoveSelection.Size = new System.Drawing.Size(130, 27);
            this.btnRemoveSelection.TabIndex = 3;
            this.btnRemoveSelection.Text = "Снять выделение";
            this.btnRemoveSelection.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnRemoveSelection.UseVisualStyleBackColor = true;
            this.btnRemoveSelection.Click += new System.EventHandler(this.ButtonRemoveSelection_Click);
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.Image = ((System.Drawing.Image)(resources.GetObject("btnSelectAll.Image")));
            this.btnSelectAll.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSelectAll.Location = new System.Drawing.Point(146, 12);
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.Size = new System.Drawing.Size(130, 27);
            this.btnSelectAll.TabIndex = 2;
            this.btnSelectAll.Text = "Выделить все";
            this.btnSelectAll.UseVisualStyleBackColor = true;
            this.btnSelectAll.Click += new System.EventHandler(this.ButtonSelectAll_Click);
            // 
            // btnAbout
            // 
            this.btnAbout.Cursor = System.Windows.Forms.Cursors.Help;
            this.btnAbout.FlatAppearance.BorderSize = 0;
            this.btnAbout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAbout.Location = new System.Drawing.Point(418, 13);
            this.btnAbout.Name = "btnAbout";
            this.btnAbout.Size = new System.Drawing.Size(21, 26);
            this.btnAbout.TabIndex = 9;
            this.btnAbout.Text = "?";
            this.btnAbout.UseVisualStyleBackColor = true;
            this.btnAbout.Click += new System.EventHandler(this.ButtonAbout_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(854, 461);
            this.Controls.Add(this.btnAbout);
            this.Controls.Add(this.btnSmartSelect);
            this.Controls.Add(this.btnExpand);
            this.Controls.Add(this.InfoBox);
            this.Controls.Add(this.btnPurgeAll);
            this.Controls.Add(this.btnRemoveSelection);
            this.Controls.Add(this.btnSelectAll);
            this.Controls.Add(this.btnClearCache);
            this.Controls.Add(this.ListBases);
            this.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormMain";
            this.Text = "Чистка кэша 1С";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnClearCache;
        private System.Windows.Forms.Button btnSelectAll;
        private System.Windows.Forms.Button btnRemoveSelection;
        private System.Windows.Forms.Button btnPurgeAll;
        private System.Windows.Forms.TextBox InfoBox;
        private System.Windows.Forms.Button btnExpand;
        private System.Windows.Forms.CheckedListBox ListBases;
        private System.Windows.Forms.Button btnSmartSelect;
        private System.Windows.Forms.Button btnAbout;
    }
}

