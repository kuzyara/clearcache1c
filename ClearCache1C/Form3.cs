﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClearCache1C
{
    public partial class FormAbout : Form
    {
        public FormAbout()
        {
            StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
        }

        private void LinkContact_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            string url;
            if (e.Link.LinkData != null)
                url = e.Link.LinkData.ToString();
            else
                url = linkContact.Text.Substring(e.Link.Start, e.Link.Length);

            if (!url.Contains("://"))
                url = "mailto:" + url;

            System.Diagnostics.Process.Start(url);
            linkContact.LinkVisited = true;
        }

        private void linkGit_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            string url;
            if (e.Link.LinkData != null)
                url = e.Link.LinkData.ToString();
            else
                url = linkGit.Text.Substring(e.Link.Start, e.Link.Length);

            if (!url.Contains("://"))
                url = "mailto:" + url;

            System.Diagnostics.Process.Start(url);
            linkGit.LinkVisited = true;
        }

        private void pictureBoxAi_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://ai-frame.net/");
        }
    }
}
