﻿namespace ClearCache1C
{
    partial class FormAbout
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAbout));
            this.pictureBoxAi = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBoxInfo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.linkContact = new System.Windows.Forms.LinkLabel();
            this.linkGit = new System.Windows.Forms.LinkLabel();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAi)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBoxAi
            // 
            this.pictureBoxAi.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxAi.Image = global::ClearCache1C.Properties.Resources.ai_full_logo___clean;
            this.pictureBoxAi.InitialImage = global::ClearCache1C.Properties.Resources.ai_full_logo___clean;
            this.pictureBoxAi.Location = new System.Drawing.Point(12, 12);
            this.pictureBoxAi.Name = "pictureBoxAi";
            this.pictureBoxAi.Size = new System.Drawing.Size(127, 119);
            this.pictureBoxAi.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxAi.TabIndex = 0;
            this.pictureBoxAi.TabStop = false;
            this.pictureBoxAi.Click += new System.EventHandler(this.pictureBoxAi_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.textBoxInfo);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(147, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 118);
            this.panel1.TabIndex = 2;
            // 
            // textBoxInfo
            // 
            this.textBoxInfo.BackColor = System.Drawing.SystemColors.Control;
            this.textBoxInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxInfo.Cursor = System.Windows.Forms.Cursors.Default;
            this.textBoxInfo.Enabled = false;
            this.textBoxInfo.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBoxInfo.Location = new System.Drawing.Point(4, 28);
            this.textBoxInfo.Multiline = true;
            this.textBoxInfo.Name = "textBoxInfo";
            this.textBoxInfo.ReadOnly = true;
            this.textBoxInfo.Size = new System.Drawing.Size(193, 87);
            this.textBoxInfo.TabIndex = 2;
            this.textBoxInfo.TabStop = false;
            this.textBoxInfo.Text = "Все просто.\r\nПрограмма читает пользовательский файл ibases.v8i и на его основе ст" +
    "роит список баз.\r\nПо найденным ID и происходит вся магия удалений.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(49, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Чистка кэша 1С";
            // 
            // linkContact
            // 
            this.linkContact.AutoSize = true;
            this.linkContact.LinkArea = new System.Windows.Forms.LinkArea(20, 15);
            this.linkContact.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.linkContact.LinkColor = System.Drawing.SystemColors.HotTrack;
            this.linkContact.Location = new System.Drawing.Point(12, 134);
            this.linkContact.Name = "linkContact";
            this.linkContact.Size = new System.Drawing.Size(199, 17);
            this.linkContact.TabIndex = 6;
            this.linkContact.TabStop = true;
            this.linkContact.Text = "Автор: FuketsuBaka (ai@ai-frame.net)";
            this.linkContact.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.linkContact.UseCompatibleTextRendering = true;
            this.linkContact.VisitedLinkColor = System.Drawing.SystemColors.WindowFrame;
            this.linkContact.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkContact_LinkClicked);
            // 
            // linkGit
            // 
            this.linkGit.AutoSize = true;
            this.linkGit.LinkArea = new System.Windows.Forms.LinkArea(5, 46);
            this.linkGit.LinkColor = System.Drawing.SystemColors.HotTrack;
            this.linkGit.Location = new System.Drawing.Point(12, 151);
            this.linkGit.Name = "linkGit";
            this.linkGit.Size = new System.Drawing.Size(267, 17);
            this.linkGit.TabIndex = 7;
            this.linkGit.TabStop = true;
            this.linkGit.Text = "GIT: https://bitbucket.org/FuketsuBaka/clearcache1c";
            this.linkGit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.linkGit.UseCompatibleTextRendering = true;
            this.linkGit.VisitedLinkColor = System.Drawing.SystemColors.WindowFrame;
            this.linkGit.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkGit_LinkClicked);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label4.Location = new System.Drawing.Point(149, 175);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "2018 г.";
            // 
            // FormAbout
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(356, 196);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.linkGit);
            this.Controls.Add(this.linkContact);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBoxAi);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormAbout";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "About";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAi)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxAi;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBoxInfo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel linkContact;
        private System.Windows.Forms.LinkLabel linkGit;
        private System.Windows.Forms.Label label4;
    }
}